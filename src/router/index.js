import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [{
    path: '/',
    name: 'Home',
    component: () => import('../pages/home'),
    meta: {
      title: 'Домашняя',
    },
  }, {
    path: '/books',
    component: () => import('../pages/books'),
    children: [{
      path: 'search',
      name: 'booksSearch',
      component: () => import('../components/books/search'),
      meta: {
        title: 'Поиск книг',
      },
    }, {
      path: 'create',
      name: 'booksCreate',
      component: () => import('../components/books/create'),
      meta: {
        title: 'Создание книги',
      },
    }, {
      path: 'view/:id',
      props: true,
      name: 'bookView',
      component: () => import('../components/books/view'),
      meta: {
        title: 'Просмотр книги',
      },
    }, {
      path: 'edit/:id',
      props: true,
      name: 'bookEdit',
      component: () => import('../components/books/edit'),
      meta: {
        title: 'Просмотр книги',
      },
    }, {
      path: '*',
      redirect: '/books/search',
    }],
  }, {
    path: '*',
    redirect: '/',
  }],
});
