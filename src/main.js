// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import { TablePlugin, FormFilePlugin } from 'bootstrap-vue';
import App from './App';
import router from './router';
import globalMixin from './mixins/globalMixin';
import store from './store/index';

Vue.use(TablePlugin);
Vue.use(FormFilePlugin);
Vue.mixin(globalMixin);

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
});
