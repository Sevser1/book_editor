export const UPDATE_BOOKS = (state, books = []) => {
  state.books = books;
};

export const SET_BOOK = (state, book = null) => {
  state.book = book;
};

export const SET_EDITED_BOOK = (state, book = null) => {
  state.editedBook = book;
};

export const UPDATE_EDITED_BOOK = (state, { field, value }) => {
  state.editedBook = {
    ...state.editedBook,
    [field]: value,
  };
};

export const DISCARD_CHANGES_EDITED_BOOK = (state) => {
  state.editedBook = null;
};
