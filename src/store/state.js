import localStorageManager from '../utills/localStorageManager';
import { clone } from '../utills/functions';

export default function () {
  const currentStorage = localStorageManager.getCurrentStorage();

  let book = null;
  let editedBook = null;

  if (/view\/\d/.test(window.location.pathname)) {
    const bookId = parseInt(window.location.pathname.match(/\d{1,}/)[0], 10);
    const foundBook = currentStorage.books.find(({ id }) => id === bookId);
    if (foundBook) {
      book = clone(foundBook);
    }
  }
  if (/edit\/\d/.test(window.location.pathname)) {
    const bookId = parseInt(window.location.pathname.match(/\d{1,}/)[0], 10);
    const foundBook = currentStorage.books.find(({ id }) => id === bookId);
    if (foundBook) {
      editedBook = clone(foundBook);
    }
  }

  return {
    books: currentStorage.books || [],
    book,
    editedBook,
  };
}
