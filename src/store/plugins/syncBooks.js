import localStorageManager from '../../utills/localStorageManager';

export default (store) => {
  // вызывается после инициализации хранилища
  store.subscribe((mutation) => {
    if (mutation.type === 'UPDATE_BOOKS') {
      localStorageManager.updateBooks(mutation.payload);
    }
  });
};
