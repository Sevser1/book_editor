import faker from 'faker';

import * as utills from '../utills/functions';

export const saveEditedBook = ({ commit, getters }) => {
  const books = utills.clone(getters.books);
  const newBook = utills.clone(getters.editedBook);
  const index = books.findIndex(({ id }) => newBook.id === id);
  if (index + 1) {
    books.splice(index, 1, newBook);
  } else {
    books.push(newBook);
  }
  commit('UPDATE_BOOKS', books);
};

export const generateBooks = ({ commit }) => {
  const books = [];
  for (let i = 0; i < 10; i += 1) {
    books.push({
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      header: faker.name.jobTitle(),
      publisher: faker.name.jobDescriptor(),
      pages: faker.random.number(10000),
      publishYear: faker.date.past(),
      editionYear: faker.date.past(),
      image: faker.image.cats(),
      id: i,
    });
  }
  commit('UPDATE_BOOKS', books);
};

export const createBook = ({ commit, getters }, newBook) => {
  const id = Math.floor(Math.random() * 999999);
  const books = utills.clone(getters.books);
  books.push({
    ...newBook,
    id,
  });
  commit('UPDATE_BOOKS', books);
};

export const removeBook = ({ commit, getters }, removeBookId) => {
  const books = utills.clone(getters.books);
  const index = books.find(({ id }) => removeBookId === id);
  books.splice(index, 1);
  commit('UPDATE_BOOKS', books);
};

