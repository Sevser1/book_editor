class LocalStorageManager {
  getCurrentStorage() {
    const state = localStorage.getItem('state');
    if (state) {
      return JSON.parse(state);
    }
    return {};
  }
  updateStorage(newState = {}) {
    const state = {
      ...this.getCurrentStorage(),
      ...newState,
    };
    localStorage.setItem('state', JSON.stringify(state));
    return this;
  }
  updateBooks(books = []) {
    return this.updateStorage({ books });
  }
}

if (!window.LocalStorageManager) {
  window.LocalStorageManager = new LocalStorageManager();
}

export default window.LocalStorageManager;
