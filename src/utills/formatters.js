import moment from 'moment';

const formatDate = (date, format = 'DD.MM.YYYY') => {
  const formattedDate = moment(date);
  return formattedDate.isValid() ? formattedDate.format(format) : null;
};

// eslint-disable-next-line import/prefer-default-export
export { formatDate };
