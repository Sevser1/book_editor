const validateHeader = header => typeof header === 'string'
  && header.length > 0
  && header.length <= 20;

const validateFirstName = firstName => typeof firstName === 'string'
  && firstName.length > 0
  && firstName.length <= 20;

const validateLastName = lastName => typeof lastName === 'string'
  && lastName.length > 0
  && lastName.length <= 20;

const validatePublisher = publisher => typeof publisher === 'string'
  && publisher.length > 0
  && publisher.length <= 30;

const validateBook = book => validateHeader(book.header)
  && validateFirstName(book.firstName)
  && validateLastName(book.lastName)
  && validatePublisher(book.publisher);

const validatePagesCount = pageCount =>
  typeof pageCount === 'number' && pageCount > 0 && pageCount < 10000;

const validatePublishYear = publishYear =>
  publishYear instanceof Date && publishYear.getFullYear() > 1800;

const validateEditionYear = editionYear =>
  editionYear instanceof Date && editionYear.getFullYear() > 1800;

export {
  validateHeader,
  validateFirstName,
  validateLastName,
  validatePublisher,
  validateBook,
  validatePagesCount,
  validatePublishYear,
  validateEditionYear,
};

